# README.md File

#### Descripción
Esto es la descripción del **proyecto** que hemos realizado para _M08_. 
Consiste en una serie de preguntas y respuestas.

#### Requisitos Técnicos
1. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
2. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
3. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
4. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
5. Nullam dictum felis eu pede mollis pretium.
6. Integer tincidunt. Cras dapibus.

#### Autores
Los autores del proyecto son:
- Alejandro Berga
- Albert Exposito
- Aitor Romero