﻿using System.Collections.Generic;

namespace Quiz.Models
{
    public class Data
    {
        public Data()
        {
            Preguntas = new List<Pregunta>();
            
            Preguntas.Add(new Pregunta("¿En que año fue 1 + 1?", new[] {"El fantástico Ralph", "2", "1996"}, 0));
            Preguntas.Add(new Pregunta("¿Para empezar al mortal kombat que es mejor pillarse?", new[] {"Ryu o Kalina","Scorpio o Subzero","Primark o Lidl"}, 1));
            Preguntas.Add(new Pregunta("¿Se escribe habeces o habeses", new[] {"Depende lo bruto que seas", "De ninguna de las dos maneras", "Yo que se jajaja"}, 0));
            Preguntas.Add(new Pregunta("De que color es la fanta de limon", new[] {"Amarillo","Naranja","Rosa"}, 1));
        }

        public List<Pregunta> Preguntas { get; set; }
        
    }

    public class Pregunta
    {
        public Pregunta(string enunciado, string[] opciones, int indiceCorrecto)
        {
            Enunciado = enunciado;
            Opciones = opciones;
            Indice = indiceCorrecto;
        }

        public string Enunciado { get; set; }
        public string[] Opciones { get; set; }
        public int Indice { get; set; }
    }
}